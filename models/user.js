const mongoose = require('mongoose')

module.exports = mongoose.model('users', mongoose.Schema({
    username: String,
    password: String,
    role: String,
    role_name: String,
    avatar: String,
    desc: String,
    status: { type: Number, default: 1 },
    create_time: { type: Number, default: Date.now() },
}))