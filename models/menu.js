const mongoose = require('mongoose')

module.exports = mongoose.model('menus', mongoose.Schema({
    module: Number,
    path: String,
    text: String,
    icon: String,
    component: String,
    pid: String
}))