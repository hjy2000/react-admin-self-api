const menuModel = require('../models/menu')

class Menu {

    static async addMenu(ctx) {
        // 当module=0时 表示添加模块  module=1时 表示添加模块下的菜单
        let { module, pid, path, text, icon, component } = ctx.request.body

        // path不能重复
        if (path) {
            const haspath = await menuModel.findOne({ path })
            if (haspath && haspath._id) {
                return ctx.body = { code: 4, msg: '路由path不能重复', data: {} }
            }
        }
        const ele = {
            module: Number(module),
            pid: (pid || ''),
            path: (path || ''),
            text: (text || ''),
            icon: (icon || ''),
            component: (component || ''),
        }

        await menuModel.insertMany([ele])
        ctx.body = { code: 0, msg: 'success', data: {} }
    }

    static async listMenu(ctx) {
        let list = await menuModel.find({})
        ctx.body = { code: 0, msg: 'success', data: { list } }
    }
}


module.exports = Menu