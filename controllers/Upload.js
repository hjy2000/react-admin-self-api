const fs = require('fs')
const path = require('path')

class Upload {
    static async imgUpload(ctx) {
        const img = ctx.request.files.img
        const readStream = fs.createReadStream(img.path)

        const cdn = path.resolve(__dirname, '../public/cdn')
        const name = `/${Date.now()}-${img.name}`
        const writeStream = fs.createWriteStream(cdn + name)

        await readStream.pipe(writeStream)
        ctx.body = { err: 0, msg: 'success', data: { img: '/cdn' + name } }
    }
}
module.exports = Upload