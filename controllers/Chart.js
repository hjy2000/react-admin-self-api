class Chart {
    static getChart(ctx) {
        ctx.body = {
            code: 0,
            msg: 'success',
            data: {
                sales: [
                    { _id: 10, subject: 'HTML5', count: 5000 },
                    { _id: 11, subject: 'Java', count: 1000 },
                    { _id: 12, subject: 'Python', count: 800 },
                    { _id: 13, subject: 'UI', count: 300 },
                    { _id: 14, subject: '大数据', count: 4000 },
                ]
            }
        }
    }
}

module.exports = Chart