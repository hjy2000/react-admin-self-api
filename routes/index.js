const USER = require('../controllers/User')
const ROLE = require('../controllers/Role')
const MENU = require('../controllers/Menu')
const UP = require('../controllers/Upload')
const ARTICLE = require('../controllers/Article')
const CHART = require('../controllers/Chart')
const checkToken = require('../middleware/checkToken')

const KoaRouter = require('@koa/router')
const router = new KoaRouter()

const v = '/api'

router
    .post(`${v}/login`, USER.login)
    .get(`${v}/user/info`, checkToken, USER.getUserInfo)
    .post(`${v}/user/add`, checkToken, USER.addUser)
    .get(`${v}/user/status`, checkToken, USER.updUser)
    .get(`${v}/user/list`, checkToken, USER.userList)
    .get(`${v}/user/del`, checkToken, USER.userDel)

    .post(`${v}/role/add`, checkToken, ROLE.addOrEditRole)
    .get(`${v}/role/list`, checkToken, ROLE.listRole)
    .get(`${v}/role/del`, checkToken, ROLE.delRole)

    .post(`${v}/menu/add`, checkToken, MENU.addMenu)
    .get(`${v}/menu/list`, checkToken, MENU.listMenu)

    .post(`${v}/article/upload`, checkToken, UP.imgUpload)
    .post(`${v}/article/update`, checkToken, ARTICLE.addArticle)
    .get(`${v}/article/list`, checkToken, ARTICLE.listArticle)
    .get(`${v}/article/info`, checkToken, ARTICLE.InfoArticle)

    .get(`${v}/chart`, checkToken, CHART.getChart)


module.exports = router