const jwt = require('jsonwebtoken')

function createToken(user) {
    return jwt.sign(
        {
            data: user,
            iat: Math.floor(Date.now() / 1000) + 3600 * 24 * 7
        },
        'hjy'
    )
}

function verifyToken(ctx) {
    return new Promise((resolve, reject) => {
        const token = ctx.headers.authorization
        try {
            var decoded = jwt.verify(token, 'hjy')
            resolve(decoded.data)
        } catch (err) {
            console.log(err)
        }
    })
}

module.exports = {
    createToken,
    verifyToken
}