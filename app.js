const Koa = require('koa')
const path = require('path')
const app = new Koa()

require('./utils/connect')

app.use(require('koa-static')(path.resolve(__dirname, 'public')))

app.use(require('koa-body')({ multipart: true }))

app.use(require('./routes/index').routes())

app.listen(3333, () => { console.log('port 3333 is running') })